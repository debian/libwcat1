PKGNAME= libwcat
PKGVERSION= 1.1

PREFIX ?= "/usr/local"

CFLAGS+= -O2 -Wall -pedantic -fPIC
#CFLAGS+= -O2 -Wall -pedantic -fPIC -ggdb
MAJOR=1
VER=1.1

all: libwcat.so libwcat.a

libwcat.so: watchcat.o
	$(CC) -shared -Wl,-soname,libwcat.so.1 -olibwcat.so.$(MAJOR).$(VER) watchcat.o -lc

libwcat.a: watchcat.o
	$(AR) cru libwcat.a watchcat.o
	ranlib libwcat.a

watchcat.o: watchcat.h watchcat.c
	$(CC) -c watchcat.c $(CFLAGS)

install:
	strip libwcat.so.$(MAJOR).$(VER)
	install -m0644 libwcat.so.$(MAJOR).$(VER) $(PREFIX)/lib/
	ln -sf libwcat.so.$(MAJOR).$(VER) $(PREFIX)/lib/libwcat.so
	ln -sf libwcat.so.$(MAJOR).$(VER) $(PREFIX)/lib/libwcat.so.$(MAJOR)
	install -m0644 watchcat.h $(PREFIX)/include/
	if [ `basename ${PWD}` != "$(PKGNAME)-$(PKGVERSION)" ]; then \
	  rm -rf ../$(PKGNAME)-$(PKGVERSION); \
	fi

clean:
	rm -f *.o *.c~ *.h~ *.conf~ *.[0-9]~ *.so.* *.a
	rm -f COPYRIGHT~ TODO~ Makefile~ *.spec~

rpm: clean
	if [ `basename ${PWD}` != "$(PKGNAME)-$(PKGVERSION)" ]; then \
	  cp -a . ../$(PKGNAME)-$(PKGVERSION); \
	fi
	tar -jcC .. --exclude CVS -f $(PKGNAME)-$(PKGVERSION).tar.bz2 $(PKGNAME)-$(PKGVERSION)
	rpm -ta $(PKGNAME)-$(PKGVERSION).tar.bz2
	rm -rf $(PKGNAME)-$(PKGVERSION).tar.bz2
	if [ `basename ${PWD}` != "$(PKGNAME)-$(PKGVERSION)" ]; then \
	  rm -rf ../$(PKGNAME)-$(PKGVERSION); \
	fi
